(module
 poulette
 (run-game)
 (import scheme
         (prefix sdl2 "sdl2:")
	 (prefix sdl2-image "img:")
         (chicken base)
         (chicken condition)
         (chicken format)
	 (chicken keyword)
         (chicken pretty-print)
         (chicken process-context) ; For command-line args
         (srfi 1)
         simple-logger
         miscmacros)

 ;; Log level set to DEBUG
 (log-level 10)
 
 (define +background-color+ (sdl2:make-color 0 0 0))
 (define +default-name+ "Poulette")
 (define +screen-size+ '(640 . 480))

 (define (kw key args default)
   (get-keyword key args (lambda () default)))

 (define (do-nothing . _) #f)
 (define (default-event-handler event quit-game!) #f)

 (define (event-of-type? event types)
   (member (sdl2:event-type event) types))

 (define (handle-event event quit-game! handlers)
   "Handle incoming events. Mostly by delegating to the `handlers' alist."
   (when event
     (when (and (sdl2:window-event? event) (eq? (sdl2:window-event-event event) 'close))
       (log-info "Closing game window")
       (quit-game!))
     (cond ((event-of-type? event '(key-down))
		    ((cdr (assoc 'keydown handlers)) event quit-game!))
	       ((event-of-type? event '(key-up))
		    ((cdr (assoc 'keyup handlers)) event quit-game!)))))


 (define (run-game . args)
   (sdl2:set-main-ready!)
   (sdl2:init! '(video))
   (img:init! '(png))

   (on-exit sdl2:quit!)

   ;; Install a custom exception handler thatx will call quit! and then
   ;; call the original exception handler. This ensures that quit! will
   ;; be called even if an unhandled exception reaches the top level.
   (current-exception-handler
    (let ((original-handler (current-exception-handler)))
      (lambda (exception)
        (sdl2:quit!)
        (original-handler exception))))

   (when (and (kw #:vsync? args #f)
	      (not (kw #:software? args #f))) ; vsync doesn't work with software rendering
     (sdl2:set-hint! 'render-vsync "1"))

   (let* ((window (sdl2:create-window!
                   (kw #:title args +default-name+)
                   'centered 'centered
				   (car (kw #:size args +screen-size+))
				   (cdr (kw #:size args +screen-size+))
                   (if (kw #:fullscreen? args #f)
					   '(fullscreen)
					   '())))
	      (renderer (sdl2:create-renderer!
                     window
                     -1
                     (if (kw #:software? args #f)
					     '(software)
					     '(accelerated)))))
     ;; Wipe the screen
     (sdl2:render-draw-color-set! renderer +background-color+)
     (sdl2:render-clear! renderer)

     ;; Load assets etc.
     ((kw #:load args do-nothing) renderer window sdl2:quit!)

     ;; Start the main loop
     (let/cc
      quit-game!
      (while #t
	    ;; Figure out how to measure the loop interval without vsync
	    ((kw #:update args do-nothing) 10 quit-game!)
	    ((kw #:draw args do-nothing) renderer)

	    ;; Handle all pending events.
	    (sdl2:pump-events!)
        ;; TODO: Rewrite this eyesore with a macro maybe?
        (let ((event (sdl2:make-event)))
	      (while (sdl2:has-events?)
	        (handle-event (sdl2:poll-event! event)
			      (lambda () (quit-game! #f))
			      (list (cons 'keydown (kw #:keydown args default-event-handler))
			            (cons 'keyup (kw #:keyup args default-event-handler))))))

	    ;; Present (i.e. show) any changes to the screen.
	    (sdl2:render-present! renderer)

	    ;; TODO: Calculate delay based on fps and sdl timer
	    (sdl2:delay! 10))))))
