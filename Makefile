CHICKEN ?= csc
CHICKEN_INTERPRET ?= csi
MODULES=poulette/color.scm \
	poulette/sprite.scm \
	poulette/text.scm
MODULES_SO=poulette.color.so \
	poulette.sprite.so \
	poulette.text.so

all: poulette.so

poulette.color.so: poulette/color.scm
	$(CHICKEN) -s -J -O3 -o $@ $<

poulette.text.so: poulette/text.scm
	$(CHICKEN) -s -J -O3 -o $@ $<

poulette.sprite.so: poulette/sprite.scm
	$(CHICKEN) -s -J -O3 -o $@ $<

poulette.so: poulette.scm
	$(CHICKEN) -s -J -O3 -o $@ $<

demo: demo.scm poulette.so $(MODULES_SO)
	$(CHICKEN) -O3 -o $@ demo.scm

