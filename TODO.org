* Poulette TODO

** Implement spritesheets support
*** TODO Investigate a texture atlas [[https://www.parallelrealities.co.uk/tutorials/atlas/atlas4.php][implementation in SDL]]
*** TODO Automatically generate sprite atlasses from image + tile details
*** TODO Implement tile struct blitting

Tiles are structs with a x,y,w,h and a reference to an origin texture.
The tile is turned into a SDL Rect and is rendered onto a destination
texture.

#+begin_src C
  void blitAtlasImage(AtlasImage *atlasImage, int x, int y, int center)
  {
      SDL_Rect dest;

      dest.x = x;
      dest.y = y;
      dest.w = atlasImage->rect.w;
      dest.h = atlasImage->rect.h;

      if (center)
      {
	  dest.x -= (dest.w / 2);
	  dest.y -= (dest.h / 2);
      }

      SDL_RenderCopy(app.renderer, atlasImage->texture, &atlasImage->rect, &dest);
  }
#+end_src

** Add an object to carry the graphic state of the game
*** Has the renderer
*** Has the fonts
