(import (poulette)
	(poulette text)
	(poulette color))

(define *font* #f)

(define (load-assets renderer window quit!)
  (set! *font* (load-font "DejaVuSans.ttf" 12)))

(define (update interval quit!)
  ;; Shuffle game around
  #f
  )

(define (draw renderer)
  (draw-text! renderer *font* "Poulette!" white '(0 . 0))
  )

(define (keydown event quit!)
  ;; Handle input
  #f
  )

(define (keyup event quit!)
  ;; Handle input
  #f
  )

(run-game #:title "Poulette Demo"
	  #:size '(320 . 240)
	  #:vsync? #t
	  #:software? #f
	  #:load load-assets
	  #:update update
	  #:draw draw
	  #:keydown keydown
	  #:keyup keyup)
