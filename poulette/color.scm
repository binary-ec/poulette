(module (poulette color)
    (white
     black
     red
     green
     blue
     yellow
     magenta
     cyan)
  (import scheme
	  (prefix sdl2 "sdl2:"))
  
  (define white (sdl2:make-color 255 255 255))
  (define black (sdl2:make-color 0 0 0))
  (define red (sdl2:make-color 255 0 0))
  (define green (sdl2:make-color 0 255 0))
  (define blue (sdl2:make-color 0 0 255))
  (define yellow (sdl2:make-color 255 255 0))
  (define magenta (sdl2:make-color 255 0 255))
  (define cyan (sdl2:make-color 0 255 255)))
