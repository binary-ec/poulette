(module (poulette sprite)
    (load-image
     draw-sprite!)
  (import scheme
	  (prefix sdl2 "sdl2:")
	  (prefix sdl2-image "img:"))

  (define (load-image renderer path)
    (sdl2:create-texture-from-surface renderer path))

  (define (sprite->rect sprite pos)
    (sdl2:make-rect (car pos)
		    (cdr pos)
		    (sdl2:texture-w sprite)
		    (sdl2:texture-h sprite)))

  (define (draw-sprite! renderer sprite pos)
    (sdl2:render-copy! renderer sprite #f (sprite->rect sprite pos))))
