(module (poulette text)
    (load-font
     draw-text!)
  (import scheme
	  miscmacros
	  (chicken base)
	  (prefix sdl2 "sdl2:")
	  (prefix sdl2-ttf "ttf:"))

  (ttf:init!)

  (define (load-font path size)
    (ttf:open-font path size))
  
  (define (draw-text! renderer font text color dest)
    (let*-values (((text-w text-h) (ttf:size-text font text))
		  ((text-surf) (sdl2:create-texture-from-surface
				renderer
				(ttf:render-text-solid font text color)))
		  ((dest-rect) (sdl2:make-rect 0 0 text-w text-h)))
      (sdl2:render-copy! renderer text-surf #f dest-rect))))
